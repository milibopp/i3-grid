with import <nixpkgs> {};

let
  rpathLibs = with pkgs; [
    xorg.libX11
    xorg.libXcursor
    xorg.libXi
    xorg.libXrandr
    xorg.libxcb
    libGL
  ];

in mkShell {
  buildInputs = with pkgs; [
    cargo
    cargo-edit
    rustc
  ] ++ rpathLibs;

  LD_LIBRARY_PATH = stdenv.lib.makeLibraryPath rpathLibs;
}
