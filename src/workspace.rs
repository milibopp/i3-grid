use crate::coordinate::{Coordinate, parse_coordinate};
use failure::Error;
use serde::Deserialize;
use std::process::Command;

#[derive(Debug, Deserialize)]
struct RawWorkspace {
    name: String,
    focused: bool,
}

impl RawWorkspace {
    fn parse(&self) -> Result<Workspace, Error> {
        let coordinate = parse_coordinate(&self.name)?;
        Ok(Workspace {
            coordinate,
            focused: self.focused,
        })
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Workspace {
    pub coordinate: Coordinate,
    pub focused: bool,
}

pub fn get_workspaces() -> Result<Vec<Workspace>, Error> {
    let output = Command::new("i3-msg")
        .args(&["-t", "get_workspaces"])
        .output()?;
    let raw: Vec<RawWorkspace> = serde_json::from_slice(&output.stdout)?;
    let workspaces = raw.into_iter()
        .filter_map(|raw| raw.parse().ok())
        .collect();
    Ok(workspaces)
}

pub fn move_to_workspace(coordinate: Coordinate) -> Result<(), Error> {
    let _output = Command::new("i3-msg")
        .arg(format!("workspace {}", crate::coordinate::write_coordinate(coordinate)))
        .output()?;
    Ok(())
}
