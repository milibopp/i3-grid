use glium::{glutin, Surface};
use glium::glutin::platform::unix::{WindowBuilderExtUnix, XWindowType};
use glium::index::PrimitiveType;
use crate::shader::{colored_mesh_shader, uniform, translate, Vertex};

pub struct Item {
    pub position: [f32; 2],
}

pub fn show_items(items: Vec<Item>) {
    let event_loop = glutin::event_loop::EventLoop::new();
    let window_builder = glutin::window::WindowBuilder::new()
        .with_decorations(false)
        .with_transparent(true)
        .with_x11_window_type(vec![XWindowType::Dialog]);
    let context_builder = glutin::ContextBuilder::new();
    let display = glium::Display::new(window_builder, context_builder, &event_loop).unwrap();

    let vertex_buffer = glium::VertexBuffer::new(&display, &[
        Vertex { position: [-0.05, -0.07], color: [0.2, 0.3, 0.4] },
        Vertex { position: [0.0, 0.07], color: [0.2, 0.3, 0.4] },
        Vertex { position: [0.05, -0.07], color: [0.2, 0.3, 0.4] },
    ]).unwrap();

    let index_buffer = glium::IndexBuffer::new(
        &display, PrimitiveType::TrianglesList, &[0u16, 1, 2]).unwrap();

    let program = colored_mesh_shader(&display);

    let draw = move || {
        let mut target = display.draw();
        target.clear_color(0.2, 0.0, 0.2, 0.2);
        for item in &items {
            target.draw(
                &vertex_buffer, &index_buffer, &program,
                &uniform(translate(item.position)),
                &Default::default()
            ).unwrap();
        }
        target.finish().unwrap();
    };

    run_event_loop(event_loop, draw);
}

fn run_event_loop(event_loop: glutin::event_loop::EventLoop<()>, mut draw: impl FnMut() + 'static) {
    use glutin::{
        event::{Event::WindowEvent, WindowEvent::*},
        event_loop::ControlFlow,
    };
    use std::time::{Instant, Duration};

    draw();
    let start_time = Instant::now();

    event_loop.run(move |event, _, control_flow| {
        let now = Instant::now();
        let next_frame_time = now + Duration::from_millis(1_000 / 60);
        if now > start_time + Duration::from_millis(750) {
            *control_flow = ControlFlow::Exit;
            return;
        }
        *control_flow = match event {
            WindowEvent { event: CloseRequested, .. } =>
                ControlFlow::Exit,

            WindowEvent { event: Focused(false), .. } =>
                ControlFlow::Exit,

            WindowEvent { event: Resized(..), .. } => {
                draw();
                ControlFlow::WaitUntil(next_frame_time)
            },

            _ =>
                ControlFlow::WaitUntil(next_frame_time),
        };
    });
}
