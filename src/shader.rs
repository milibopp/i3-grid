use glium::{implement_vertex, program, Program, uniform};

#[derive(Copy, Clone)]
pub struct Vertex {
    pub position: [f32; 2],
    pub color: [f32; 3],
}

implement_vertex!(Vertex, position, color);

type Matrix = [[f32; 4]; 4];

pub fn uniform(matrix: Matrix) -> impl glium::uniforms::Uniforms {
    uniform! { matrix: matrix }
}

pub fn translate(vector: [f32; 2]) -> Matrix {
    [
        [1.0, 0.0, 0.0, vector[0]],
        [0.0, 1.0, 0.0, vector[1]],
        [0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 1.0f32]
    ]
}

// pub fn identity() -> Matrix {
//     [
//         [1.0, 0.0, 0.0, 0.0],
//         [0.0, 1.0, 0.0, 0.0],
//         [0.0, 0.0, 1.0, 0.0],
//         [0.0, 0.0, 0.0, 1.0f32]
//     ]
// }

pub fn colored_mesh_shader(display: &glium::Display) -> Program {
    program!(display,
        140 => {
            vertex: "
                #version 140

                uniform mat4 matrix;

                in vec2 position;
                in vec3 color;

                out vec3 vColor;

                void main() {
                    gl_Position = vec4(position, 0.0, 1.0) * matrix;
                    vColor = color;
                }
            ",

            fragment: "
                #version 140
                in vec3 vColor;
                out vec4 f_color;

                void main() {
                    f_color = vec4(vColor, 1.0);
                }
            "
        }
    ).unwrap()
}
