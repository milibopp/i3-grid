use failure::{err_msg, Error};

pub type Coordinate = [i64; 2];

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Direction {
    Left,
    Right,
    Up,
    Down,
}

pub fn move_by([x, y]: Coordinate, direction: Direction) -> Coordinate {
    use Direction::*;
    match direction {
        Left => [x - 1, y],
        Right => [x + 1, y],
        Up => [x, y + 1],
        Down => [x, y - 1],
    }
}

pub fn write_coordinate(coord: Coordinate) -> String {
    format!("{}|{}", coord[0], coord[1])
}

pub fn parse_coordinate(input: &str) -> Result<Coordinate, Error> {
    use std::str::FromStr;

    let coords: Vec<_> = input
        .split("|")
        .map(i64::from_str)
        .collect();
    match coords.as_slice() {
        [Ok(x), Ok(y)] => Ok([*x, *y]),
        _ => Err(err_msg(format!("could not parse coordinates: {}", input))),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parses_coordinates() {
        assert_eq!(parse_coordinate("0|0").unwrap(), [0, 0]);
        assert_eq!(parse_coordinate("1|3").unwrap(), [1, 3]);
        assert_eq!(parse_coordinate("-11|36").unwrap(), [-11, 36]);
        assert_eq!(parse_coordinate("5|-4").unwrap(), [5, -4]);
    }
}
